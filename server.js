const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const router = require('./router/routes')

const PORT = 3000;

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(router);

app.listen(PORT, () => {
    console.log(`Server started on PORT ${PORT}`);
});