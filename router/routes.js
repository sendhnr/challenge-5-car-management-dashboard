const { Router } = require("express");
const multer = require("multer");
const router = Router();
const upload = multer();
const { getCars, createCars, getCarsById, updateCars, deleteCars } = require("../controller/carsController");

// route for API
router.post("/api", upload.single("image"), createCars);
router.get("/api/:id", getCarsById);
router.put("/api/:id", upload.single("image"), updateCars);
router.delete("/api/:id", deleteCars);
router.get("/api", getCars);

module.exports = router;
